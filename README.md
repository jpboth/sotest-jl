# A small commented test to describe a .so file from a julia file with Package compiler

The possibility to create .so files from julia code
is a great possibility of the Julia language.

I had some difficulties making it work and searching help in JuliaDiscourse I saw that it could help to put an example
with some comments.

The following example is what I grasped, possibly there are some mistakes as I do not know the inside of Julia, but at least the example works with Julia 1.1 and a Linux testing.

The main 3 difficulties I encountered were :

1. do a correct initialization of julia in the .c file (See comments in calljulia.c)
2. see I could use juliac.jl which i find more practical to be used in a global makefile.
    (and also -vas option do not copy all julia libs by default. Note that the api build_shared_lib function 
    has a copy_julialibs arg to be set to false for that).
3. link the generated .so file with the correct ld flags. (See comments in Makefile.mk)

It must be noted that the .so file generated is an extended version of JULIA_BINDIR/../lib/julia/sys.so
which is a precompiled version of Base. So it is a large file.

Please feel free to correct any mistakes.
