#include <stdio.h>
#include <stdlib.h>


// necessary to get julia_init
#define JULIA_ENABLE_THREADING


#include "uv.h"
#include "julia.h"


#ifdef JULIA_DEFINE_FAST_TLS // only available in Julia v0.7 and above
JULIA_DEFINE_FAST_TLS()
#endif

// doc for jl_init_with_image Cf file julia/master/src/jlapi.c
// First argument is the user /bin directory where the julia binary is, or NULL to guess.
// Second argument is the path of a system image file (*.ji) relative to the
// first argument path, or relative to the default julia home dir.
// The default is something like ../lib/julia/sys.ji
//
// julia_init (in task.c)  just calls _julia_init in init.c
// This function among many things (gc, threading ) convert relative path to absolute path...
// then calls jl_preload_sysimg_so(jl_options.image_file);
//
// jl_init_with_image (in jlapi.c) calls  julia_init and so we can pass an absolute path for our sys-image
//
// I copy juliac.jl from PackageCompiler dir to my test directory , it is easier.
// Running julia juliac.jl -vas --compile=all libcmyfile.jl generates a .so in builddir.
// Then we have to compile and link our .c file with the .so generated.
// For that we use Makefile.mk with the correct ld flags we got from the julia-config.jl
// in JULIA_BINDIR/../share/julia/julia-config.jl is a script giving arguments to use for ld in Makefile.
//

// my function in jl file
long long julia_main();

// to possibilities to initialize julia.
// this one mimics jl_init_with_image Cf julia/api.c
int init_1(char * sysimgpath) {
        libsupport_init();
        jl_options.image_file = sysimgpath;
        julia_init(JL_IMAGE_JULIA_HOME);
        jl_exception_clear();
        return 0;
 }

// the second directly calls jl_init_with_image
// relative path with respect to julia bin dir but in fact we can give absolute path, Cf comments above
int init_2(char * sysimgpath) {
        //  call to  libsupport_init() is useless if we call jl_init_with_image, is done in  jl_init_with_image
        // the NULL arg means use JULIA_BINDIR, else must give path to julia bindir
        jl_init_with_image(NULL, sysimgpath);
        return 0;
}

int main(int argc, char **argv) {
    //
    uv_setup_args(argc, argv); // no-op on Windows
    //
    char * sysimgpath = "/home.1/jpboth/Julia/Sotest/builddir/libcfromjl.so";
    // init_1(sysimgpath);
    init_2(sysimgpath); 
    long long ret = julia_main();
    fprintf(stdout, "\n got %lld ", ret);
}