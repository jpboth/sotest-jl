# Makefile test 
# with julia installed in /home.2/Opt/julia-1.1.1 
# bash gives: $JULIA_BINDIR/../share/julia/julia-config.jl  --ldflags  gives : -L'/home.2/Opt/julia-1.1.1/lib' -Wl,--export-dynamic
# bash gives: $JULIA_BINDIR/../share/julia/julia-config.jl  --ldlibs gives : -Wl,-rpath,'/home.2/Opt/julia-1.1.1/lib' -Wl,-rpath,'/home.2/Opt/julia-1.1.1/lib/julia' -ljulia

INCLUDE_DIR = -I/home.2/Opt/julia/include/julia
JULIA_LIB = '/home.2/Opt/julia-1.1.1/lib'
SRCDIR = .
OBJDIR= .
BINDIR = .

CPPFLAGS = $(INCLUDE_DIR) $(DEFINES)

CCFLAGS = -v  -g -fPIC -Wall $(CPPFLAGS)

COMPILE.cc = gcc -c  $(CCFLAGS) $(CPPFLAGS)  $(OPENMPFLAG)

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(COMPILE.cc) $< -o $@


LFLAGT=  -Wl,-L$(JULIA_LIB) -Wl,-L./builddir /home.1/jpboth/Julia/Sotest/builddir/libcfromjl.so -Wl,--export-dynamic   -Wl,-rpath,'/home.2/Opt/julia-1.1.1/lib' -Wl,-rpath,'/home.2/Opt/julia-1.1.1/lib/julia' -ljulia

DYNAMIC_FLAGS = -shared $(LFLAGT)
LINK.cc = gcc  $(INCLUDE_DIR) $(LFLAGT)
LINK_DYNAMIC.cc = gcc -g  $(DYNAMIC_FLAGS)


module : \
	calljulia


calljulia:
	$(COMPILE.cc)  $(SRCDIR)/calljulia.c -o $(OBJDIR)/calljulia.o
	$(LINK.cc)  -v $(OBJDIR)/calljulia.o  -o  $(BINDIR)/calljulia-g